## Readme

Caffe cpp code to train/test mnist.  


## Dependencies

Compile and install [caffe](http://caffe.berkeleyvision.org/installation.html) on the system:  

        cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local


## Instructions

1. Download the dataset:
    ```bash
    bash download_dataset.sh
    ```

2. Compile the code:  

    To train/test the code in CPU, set caffe::set_mode() accordingly.  

    ```bash
    mkdir build && cd build  
    cmake .. && make
    ```

3. Train the network:
    ```bash
    ./train -d ../dataset/ -n ../network/
    ```

4. Test:
    ```bash
    ./test -n ../network/ -w ../network/weights/lenet_iter_10000.caffemodel -i ../dataset/images_t10k-images-idx3-ubyte/000000.png
    ```


## Acknowledgement

This work is part of Thomio Watanabe PhD project funded by grant: #2015/26293-0, São Paulo Research Foundation (FAPESP).
"Opinions, hypothesis and conclusions or recommendations expressed herein are the author(s) responsibility and do not necessarily conform with FAPESP vision."  

Copyright © 2017 Thomio Watanabe  
Universidade de São Paulo  
Laboratório de Robótica Móvel

#ifndef CAFFE_MNIST_HPP_
#define CAFFE_MNIST_HPP_

#include <boost/shared_ptr.hpp>
#include <string>

#include <caffe/data_transformer.hpp>
#include <caffe/caffe.hpp>


struct MNISTData;

class CaffeMNIST
{
    public:
    CaffeMNIST( MNISTData *mnist_data, const std::string &network_dir );
    CaffeMNIST(const std::string &network_dir, const std::string &weights_file);
    ~CaffeMNIST();

    void defineNetworkParams();

    void getTransformation( const std::string &model_file );
    void loadHDF5( const std::string &file_name, unsigned max_dim );
    void loadImageMean( const std::string &mean_file );

    void printNetInfo() const;
    void printBlobInfo( const std::string &blob_name ) const;
    void printBlobContent( const std::string &blob_name ) const;
    void printImageInfo( const cv::Mat &image ) const;

    void trainMNIST();
    std::vector<float> Predict( const std::string &image_file );

    private:
    void readSolverFile( const std::string &network_dir );
    void readNetworkFile( const std::string &network_dir );
    void feedBlob( const float *input_state, const std::string &blob_name );
    void feedTestBlob( const float *input_state, const std::string &blob_name );
    std::vector<float> getBlobContent( const std::string &blob_name ) const;
    void scaleImages();

    boost::shared_ptr<caffe::Net<float> > caffeNet;
    boost::shared_ptr<caffe::Net<float> > testCaffeNet;
    boost::shared_ptr<caffe::Solver<float> > caffeSolver;
    caffe::SolverParameter solverParam;
    caffe::NetParameter networkParam;

    boost::shared_ptr<caffe::DataTransformer<float> > dataTransformer;
    caffe::Blob<float> imageMean;

    std::string imagesBlobName, labelsBlobName, outputBlobName, probBlobName;
    std::string solverFile, networkFile;

    MNISTData *mnistData;
};

#endif

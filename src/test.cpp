#include "CaffeMNIST.hpp"

#include <boost/program_options.hpp>
#include <iostream>
#include <string>


namespace po = boost::program_options;
using namespace std;



po::variables_map program_options( const int argc, const char *const argv[] )
{
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "Usage: ./train -n ../network \
            -i ../dataset/images_t10k-images-idx3-ubyte/000000.png \
            -w ../network/weights/lenet_iter_10000.caffemodel")
        ("network,n", po::value<string>(), "Path to network directory")
        ("image,i", po::value<string>(), "Path to image FILE !!!")
        ("weights,w", po::value<string>(), "Path to weights FILE !!!");

    po::variables_map args;
    po::store( po::parse_command_line(argc, argv, desc), args );
    po::notify(args);

    if( args.count("help") || argc != 7 ){
        cout << desc << endl;
        exit( EXIT_FAILURE );
    }

    cout << endl;
    if( args.count("network") ){
        cout << "Network directory: " << args["network"].as<string>() << endl;
    }

    if( args.count("image") ){
        cout << "Image file: " << args["image"].as<string>() << endl;
    }

    if( args.count("weights") ){
        cout << "Weights file: " << args["weights"].as<string>() << endl;
    }

    return args;
}


void printTop3Predictions( vector<float> prob )
{
    const unsigned vector_size = prob.size();
    vector<pair<float, unsigned> > prob_and_classes;
    for( size_t i = 0; i < vector_size; ++i )
        prob_and_classes.push_back( make_pair(prob[i], i) );

    // Reverse sorting
    sort( prob_and_classes.begin(), prob_and_classes.end() );

    cout << "\nPorb" << "\tClass" << endl;
    for( size_t i = vector_size - 1; i > vector_size - 4; --i ){
        cout << setprecision(2) << prob_and_classes[i].first <<  "\t  "
            << prob_and_classes[i].second << endl;
    }
}

void printTop1Prediction( vector<float> prob )
{
    vector<float>::iterator prob_it = max_element( prob.begin(), prob.end() );
    const unsigned prediction = distance( prob.begin(), prob_it );

    cout << "Most probably class = " << prediction << endl;
}


int main( int argc, const char *argv[] )
{
    po::variables_map args = program_options( argc, argv );

    string network_dir = args["network"].as<string>();
    string image_file = args["image"].as<string>();
    string weights_file = args["weights"].as<string>();

    CaffeMNIST caffe_mnist( network_dir, weights_file );

    vector<float> prob_distribution = caffe_mnist.Predict( image_file );

    printTop1Prediction( prob_distribution );
    printTop3Predictions( prob_distribution );

    return 0;
}

#include "MNISTData.hpp"

#include <iostream>
#include <fstream>
#include <iomanip>

#include <boost/filesystem.hpp>
#include <boost/make_shared.hpp>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


namespace fs = boost::filesystem;
using namespace std;


MNISTData::MNISTData( const string &dataset_path )
{
    string train_images_file = "train-images-idx3-ubyte";
    string test_images_file = "t10k-images-idx3-ubyte";
    string train_labels_file = "train-labels-idx1-ubyte";
    string test_labels_file = "t10k-labels-idx1-ubyte";

    trainImages = boost::make_shared<ImagesData> ( dataset_path, train_images_file );
    testImages = boost::make_shared<ImagesData> ( dataset_path, test_images_file );
    trainImages->saveImages();
    testImages->saveImages();

    trainLabels = boost::make_shared<LabelsData> ( dataset_path, train_labels_file );
    testLabels = boost::make_shared<LabelsData> ( dataset_path, test_labels_file );
    trainLabels->saveLabels();
    testLabels->saveLabels();
}




unsigned IDXFormat::reverse_bits( const unsigned &x )
{
    // The mnist database has the MSB first.
    // intel read MSB last !!!
    // For intel CPU it is required to reverse the bit order
    // Each word has 4 bytes
    unsigned first =  (x >> 24) & 0x000000FF;
    unsigned second = (x >> 8)  & 0x0000FF00;
    unsigned third =  (x << 8)  & 0x00FF0000;
    unsigned fourth = (x << 24) & 0xFF000000;

    // result is the bitwise or of the bytes
    unsigned result = fourth | third | second | first;

    return result;
}


unsigned IDXFormat::read_magic_number( const unsigned &x )
{
    // The magic number is an integer (MSB first).
    // The first 2 bytes are always 0.
    // The third byte codes the type of the data:
    //      0x08: unsigned byte
    // The 4-th byte codes the number of dimensions of the vector/matrix:
    //      1 for vectors, 2 for matrices....
    unsigned first =  (unsigned)(x >> 24) & 0x000000FF;
    unsigned second = (unsigned)(x >> 16) & 0x000000FF;
    unsigned third =   (unsigned)(x >> 8) & 0x000000FF;
    unsigned fourth =         (unsigned)x & 0x000000FF;

//    printf( "%u \n", x );
    printf( "first byte = %u \n", first );
    printf( "second byte = %u \n", second );
    printf( "third byte = %u \n", third );
    printf( "fourth byte = %u \n", fourth );

    unsigned file_type = 0;
    if( fourth == 1 ){
        cout << "Label file." << endl;
        file_type = 1;
    }

    if( fourth == 3 ){
        cout << "Image file." << endl;
        file_type = 2;
    }

    return file_type;
}




ImagesData::ImagesData( const string &dataset_path, const string &file_name )
{
    datasetPath = dataset_path;
    fileName = file_name;

    loadImages();
}


ImagesData::~ImagesData()
{
    free(images);
}


void ImagesData::loadImages( )
{
    fs::path data_path( datasetPath );
    fs::path file_path = data_path / fileName;

    FILE *file_ptr = fopen( file_path.c_str(), "rb" );
    if( file_ptr != NULL )
    {
        cout << "Reading " << file_path << endl;

        // http://yann.lecun.com/exdb/mnist/index.html
        // All files have 4bytes words.
        // The first word is the magic number
        unsigned magic_number;
        fread( &magic_number, sizeof(unsigned), 1, file_ptr );
        magic_number = IDXFormat::reverse_bits( magic_number );
        unsigned file_type = IDXFormat::read_magic_number( magic_number );

        fread( &numImages, sizeof(unsigned), 1, file_ptr );
        numImages = IDXFormat::reverse_bits( numImages );
        cout << "Number of images = " << numImages << endl;

        fread( &numRows, sizeof(unsigned), 1, file_ptr );
        numRows = IDXFormat::reverse_bits( numRows );
        cout << "Number of rows = " << numRows << endl;

        fread( &numCols, sizeof(unsigned), 1, file_ptr );
        numCols = IDXFormat::reverse_bits( numCols );
        cout << "Number of cols = " << numCols << endl;

        images = (float *) malloc( numImages * numRows * numCols *
                sizeof(float) );

        // 0 means background (white), 255 means foreground (black).
        size_t index = 0;
        while( !feof(file_ptr) ){
            unsigned char pixel;
            fread( &pixel, sizeof(unsigned char), 1, file_ptr );
            *(images + index) = (float) pixel;
            ++index;
        }
        cout << endl;
    }else{
        cout << "Error !!! " << endl;
        cout << "Could not open " << file_path << endl;
        exit( EXIT_FAILURE );
    }
}


void ImagesData::saveImages()
{
//    cv::Mat image( 28, 28, CV_8UC1, images_ptr );
//    cv::namedWindow( "Display window", CV_WINDOW_AUTOSIZE );
//    imshow( "Display window", image );
//    cv::waitKey(0);

    fs::path data_path( datasetPath );
    string directory_name = "images_" + fileName;
    fs::path output_path = data_path / directory_name;

//    if( fs::exists( output_path ) )
//        // Remove existant files
//        fs::remove_all( output_path );

    // By default only generate the images if the output directory doesn't exist
    if( ! fs::exists( output_path ) ){
        if( fs::create_directory(output_path) )
            cout << output_path << " directory created successfully." << endl;

        const unsigned num_pixels = numRows * numCols;
        for( size_t k = 0; k < numImages; ++k ){
            unsigned index = k * num_pixels;
            float * image_ptr = images + index;
            cv::Mat image( numRows, numCols, CV_32FC1, image_ptr );
            std::stringstream image_path;
            image_path << output_path.string() << "/" << std::setw(6) <<
                            std::setfill('0') << k << ".png";
            imwrite( image_path.str(), image );
        }
    }
}


// Print "images" on terminal
void ImagesData::printImages()
{
    const unsigned num_pixels = numRows * numCols;
    for( size_t k = 0; k < numImages; ++k ){
        cout << "Image = " << k << endl;
        for(size_t i = 0; i < numRows; ++i ){
            for( size_t j = 0; j < numCols; ++j ){
                unsigned index = k * num_pixels + i * numCols + j;
                unsigned value = (unsigned) images[ index ];
                if( value > 0 ){
                    cout << ". ";
                }else{
                    cout << " ";
                }
            }
            cout << endl;
        }
    }
}




LabelsData::LabelsData( const string &dataset_path, const string &file_name )
{
    datasetPath = dataset_path;
    fileName = file_name;

    loadLabels();
}


void LabelsData::loadLabels()
{
    fs::path data_path( datasetPath );
    fs::path file_path = data_path / fileName;

    FILE *file_ptr = fopen( file_path.c_str(), "rb" );
    if( file_ptr != NULL )
    {
        cout << "Reading " << file_path << endl;

        unsigned magic_number;
        fread( &magic_number, sizeof(unsigned), 1, file_ptr );
        magic_number = IDXFormat::reverse_bits( magic_number );
        unsigned file_type = IDXFormat::read_magic_number( magic_number );

        fread( &numLabels, sizeof(unsigned), 1, file_ptr );
        numLabels = IDXFormat::reverse_bits( numLabels );
        cout << "Number of labels = " << numLabels << endl;

        labels = (float *) malloc( numLabels * sizeof(float) );

        size_t index = 0;
        while( !feof(file_ptr) ){
            unsigned char label;
            fread( &label, sizeof(unsigned char), 1, file_ptr );
            *(labels + index) = (float) label;
    //        cout << "Label = " << (unsigned) *(label_data + index) << endl;
            ++index;
        }
        cout << endl;
    }else{
        cout << "Error !!! " << endl;
        cout << "Could not open " << file_path << endl;
        exit( EXIT_FAILURE );
    }
}


LabelsData::~LabelsData()
{
    free(labels);
}


void LabelsData::saveLabels()
{
    fs::path data_path( datasetPath );
    string file_name = "labels_" + fileName + ".txt";
    fs::path output_file_path = data_path / file_name;
    string output_file_name = output_file_path.string();

//    if( fs::exists( output_file_name ) )
//        // Remove existant file
//        fs::remove( output_file_name );

    // By default only generate labels file if it doesn't exist
    if( ! fs::exists( output_file_name ) ){
        cout << "Creating labels file: " << output_file_name << endl;

        std::ofstream output_file( output_file_name.c_str() );
        for( size_t k = 0; k < numLabels; ++k ){
            float * label_ptr = labels + k;
            output_file << (float) *label_ptr << endl;
        }
        output_file.close();
    }
}



#include "CaffeMNIST.hpp"
#include "MNISTData.hpp"
#include <caffe/util/hdf5.hpp>

#include <boost/filesystem.hpp>

#include <opencv/cv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace fs = boost::filesystem;
using namespace std;

using cv::Mat;
using boost::shared_ptr;
using caffe::Caffe;
using caffe::Blob;
using caffe::Net;
using caffe::BlobProto;
using caffe::NetParameter;
using caffe::LayerParameter;
using caffe::TransformationParameter;
using caffe::DataTransformer;
using caffe::Datum;



// Constructor for training
CaffeMNIST::CaffeMNIST( MNISTData *mnist_data, const string &network_dir )
{
    defineNetworkParams();

    mnistData = mnist_data;

    readSolverFile( network_dir );

    caffeSolver.reset(caffe::SolverRegistry<float>::CreateSolver(solverParam));
    // You can also pass the path to solver
    // caffeSolver.reset(caffe::SolverRegistry<float>::CreateSolver(path_to_solver));
    caffeNet = caffeSolver->net();
    testCaffeNet = caffeSolver->test_nets()[0];

    printNetInfo();

    printBlobInfo( imagesBlobName );
    printBlobInfo( labelsBlobName );
    printBlobInfo( outputBlobName );

    scaleImages();
}


// Constructor for testing
CaffeMNIST::CaffeMNIST( const string &network_dir, const string &weights_file )
{
    defineNetworkParams();

    fs::path network_path( network_dir );
    fs::path path_to_network = network_path / networkFile;

    caffeNet.reset( new Net<float>( path_to_network.string(), caffe::TEST) );
    // You could also use the networkParam variable to initialize caffeNet
    // Call readNetworkFile just to print protobuf file information
    readNetworkFile( network_dir );

    if( access( weights_file.c_str(), F_OK ) != -1  ){
        cout << "\nLoading network weights..." << endl;
        caffeNet->CopyTrainedLayersFrom( weights_file );
    }

    printNetInfo();
    printBlobInfo( imagesBlobName );
    printBlobInfo( probBlobName );
}


CaffeMNIST::~CaffeMNIST() {}


// List with all hardcoded variables -> sorry
void CaffeMNIST::defineNetworkParams()
{
    Caffe::set_mode(Caffe::GPU);

    imagesBlobName = "images";
    labelsBlobName = "labels";
    outputBlobName = "output";
    probBlobName = "prob";

    solverFile = "lenet_solver.prototxt";
    networkFile = "lenet_inference.prototxt";
}


void CaffeMNIST::readSolverFile( const string &network_dir )
{
    fs::path network_path( network_dir );
    fs::path path_to_solver = network_path / solverFile;
    caffe::ReadProtoFromTextFileOrDie( path_to_solver.string(), &solverParam );

    cout << endl;
    cout << "Solver file parameters: " << endl;
    cout << "Path to solver file = " << path_to_solver << endl;
    fs::path net_path = network_path / solverParam.net();
    solverParam.set_net( net_path.string() );
    cout << "Path to network file = " << solverParam.net() << endl;
//    cout << "Total number of iterations = " << solverParam.max_iter() << endl;
//    cout << "Learning rate = " << solverParam.base_lr() << endl;
//    cout << "Snapshot interval = " << solverParam.snapshot() << endl;
    // Check if weights directory exist and/or create it
    fs::path weights_directory = network_path / "weights";
    if( fs::create_directory(weights_directory) )
        cout << weights_directory << " directory created successfully." << endl;
    fs::path weights_prefix = network_path / "weights" /
            solverParam.snapshot_prefix();
    solverParam.set_snapshot_prefix( weights_prefix.string() );
//    cout << "Snapshot prefix = " << solverParam.snapshot_prefix() << endl;
    cout << endl;
}


void CaffeMNIST::readNetworkFile( const string &network_dir )
{
    fs::path network_path( network_dir );
    fs::path path_to_network = network_path / networkFile;

    caffe::ReadProtoFromTextFileOrDie( path_to_network.string(), &networkParam );

    // Look for the messages definition inside caffe.proto file
    // Most of these information are found within caffeNet
    const unsigned num_layers = networkParam.layer_size();
    cout << "Number of layers = " << num_layers << endl;
    for( size_t i = 0; i < num_layers; ++i ){
         caffe::LayerParameter layer_param = networkParam.layer(i);
         cout << "Layer "<< i << ", name = " << layer_param.name() << endl;
    }
}


// NOT TESTED
void CaffeMNIST::getTransformation( const string &network_file )
{
    NetParameter net_parameters;
    ReadProtoFromTextFileOrDie( network_file, &net_parameters );
    LayerParameter layer_parameters = net_parameters.layer(0);
    if( layer_parameters.has_transform_param() ){
        TransformationParameter transformation_params = layer_parameters.transform_param();
        dataTransformer.reset( new DataTransformer<float>( transformation_params, caffe::TEST ) );
    }
}


// NOT TESTED
void CaffeMNIST::loadImageMean( const string &mean_file )
{
    BlobProto mean_proto;
    ReadProtoFromBinaryFileOrDie( mean_file.c_str(), &mean_proto );

    Blob<float> mean_blob;
    mean_blob.FromProto(mean_proto);

    imageMean.CopyFrom(mean_blob, false, true);
}


// NOT TESTED
// Load hdf5 files with ONLY ONE GROUP
void CaffeMNIST::loadHDF5( const string &file_name, unsigned max_dim )
{
    Blob<float> output_blob;

    hid_t file_hid = H5Fopen( file_name.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT);
    CHECK_GE(file_hid, 0) << "Couldn't open " << file_name;

    string source_layer_name = caffe::hdf5_get_name_by_idx(file_hid, 0);
    cout << "source_layer_name = " << source_layer_name << endl;

    hdf5_load_nd_dataset( file_hid, source_layer_name.c_str(), 0, max_dim, &output_blob);

    printBlobInfo( source_layer_name );

    H5Fclose(file_hid);
}


void CaffeMNIST::printNetInfo() const
{
    cout << "\nNetwork name = " << caffeNet->name() << endl;
    cout << endl;

    vector<string> layers = caffeNet->layer_names();
    cout << "Number of layers = " << layers.size() << endl;
    for(vector<string>::iterator it = layers.begin(); it != layers.end(); ++it)
        cout << "Layer name = " << *it << endl;
    cout << endl;

    vector<string> blobs = caffeNet->blob_names();
    cout << "Number of blobs = " << blobs.size() << endl;
    for(vector<string>::iterator it = blobs.begin(); it != blobs.end(); ++it)
        cout << "Blob name = " << *it << endl;
    cout << endl;
}


void CaffeMNIST::printBlobInfo( const string &blob_name ) const
{
    shared_ptr<Blob<float> > blob = caffeNet->blob_by_name( blob_name );

    cout << "\nBlob: " << blob_name << endl;
    cout << "Number of axis = " << blob->num_axes() << endl;
    cout << "Elements count = " << blob->count() << endl;
    for( size_t i = 0; i < blob->num_axes(); ++i )
        cout << "Axis = "<< i << " num elements = " << blob->shape(i) << endl;

    cout << endl;
}


void CaffeMNIST::printBlobContent( const string &blob_name ) const
{
    shared_ptr<Blob<float> > blob = caffeNet->blob_by_name( blob_name );

    cout << "Blob: " << blob_name << endl;
    const float *content = blob->cpu_data();
    for( size_t i = 0; i < blob->count(); ++i )
        cout << i << " = " << content[i] << endl;
}


void CaffeMNIST::printImageInfo( const Mat &image ) const
{
    cout << "Depth = " << image.depth() << endl;
    cout << "Number of channels = " << image.channels() << endl;
    cout << "Number of rows = " << image.rows << endl;
    cout << "Number of collumns = " << image.cols << endl;
}


void CaffeMNIST::feedBlob( const float *input_state, const string &blob_name )
{
    shared_ptr<Blob<float> > blob_ptr = caffeNet->blob_by_name( blob_name );

    // Load blob with data;
    // The gpu memory is synced with the cpu memory -> todo: check this
    float *blob_content = blob_ptr->mutable_cpu_data();
    const unsigned input_size = blob_ptr->count();

    copy( input_state, input_state + input_size, blob_content );
}


void CaffeMNIST::feedTestBlob( const float *input_state, const string &blob_name )
{
    shared_ptr<Blob<float> > blob_ptr = testCaffeNet->blob_by_name( blob_name );

    // Load blob with data;
    // The gpu memory is synced with the cpu memory -> todo: check this
    float *blob_content = blob_ptr->mutable_cpu_data();
    const unsigned input_size = blob_ptr->count();

    copy( input_state, input_state + input_size, blob_content );
}
            


vector<float> CaffeMNIST::getBlobContent( const string &blob_name ) const
{
    shared_ptr<Blob<float> > output_blob = caffeNet->blob_by_name(blob_name);
    const float *output_data = output_blob->cpu_data();

    vector<float> output;
    const unsigned output_size = output_blob->count();
    for(size_t i = 0; i < output_size; ++i){
        output.push_back( output_data[i] );
    }

    return output;
}


void CaffeMNIST::scaleImages()
{
    float  *train_images = mnistData->trainImages->images;

    unsigned num_images = mnistData->trainImages->numImages;
    unsigned num_cols = mnistData->trainImages->numCols;
    unsigned num_rows = mnistData->trainImages->numRows;

    unsigned num_pixels = num_images * num_cols * num_rows;
    for( size_t i = 0; i < num_pixels; ++i ){
        train_images[i] = train_images[i] / 255;
    }

    float  *test_images = mnistData->testImages->images;
    num_images = mnistData->testImages->numImages;
    num_cols = mnistData->testImages->numCols;
    num_rows = mnistData->testImages->numRows;

    num_pixels = num_images * num_cols * num_rows;
    for( size_t i = 0; i < num_pixels; ++i ){
        test_images[i] = test_images[i] / 255;
    }
}


void CaffeMNIST::trainMNIST()
{
    cout << "\nTraining network... " << endl;

    shared_ptr<Blob<float> > images_blob = caffeNet->blob_by_name(imagesBlobName);
    const unsigned batch_size = images_blob->shape(0);
    cout << "Batch size = " << batch_size << endl;

    const unsigned num_images = mnistData->trainImages->numImages;
    const int index_limit = (int) num_images / batch_size;
    // num_labels must be equal to num_images
//    const unsigned num_labels = mnistData->trainLabels->numLabels;

    const unsigned num_cols = mnistData->trainImages->numCols;
    const unsigned num_rows = mnistData->trainImages->numRows;
    const unsigned images_data_size = batch_size * num_rows * num_cols;
    const unsigned labels_data_size = batch_size;

    const unsigned test_num_images = mnistData->testImages->numImages;
    const int test_index_limit = (int) test_num_images / batch_size;

    unsigned max_iterations = solverParam.max_iter();
    // test_iter is a repeated field
    unsigned test_iterations = solverParam.test_iter(0);

    // Train iteration up to max_iterations
    for( size_t images_index = 0, labels_index = 0, iteration = 0;
                iteration < max_iterations; ++iteration ){
        if( iteration % index_limit == 0 ){
            images_index = 0;
            labels_index = 0;
        }else{
            images_index += images_data_size;
            labels_index += labels_data_size;
        }
        const float *images_ptr = mnistData->trainImages->images + images_index;
        const float *labels_ptr = mnistData->trainLabels->labels + labels_index;

        feedBlob( images_ptr, imagesBlobName );
        feedBlob( labels_ptr, labelsBlobName );

        // Testing training results
        // I do not understand how caffe handle the test iteration !!! TODO
        if( iteration % solverParam.test_interval() == 0 ){
            for( size_t test_images_index = 0, test_labels_index = 0, i = 0;
                    i < test_iterations; ++i ){
                if( i % test_index_limit == 0 ){
                    test_images_index = 0;
                    test_labels_index = 0;
                }else{
                    test_images_index += images_data_size;
                    test_labels_index += labels_data_size;
                }
                const float *test_images = mnistData->testImages->images + test_images_index;
                const float *test_labels = mnistData->testLabels->labels + test_labels_index;

                feedTestBlob( test_images, imagesBlobName );
                feedTestBlob( test_labels, labelsBlobName );

                testCaffeNet->Forward();
            }
        }

        caffeSolver->Step(1);
    }
}


vector<float> CaffeMNIST::Predict( const string &image_file )
{
    cv::Mat test_image = cv::imread( image_file, CV_LOAD_IMAGE_GRAYSCALE );
    unsigned char* test_image_ptr = test_image.data;
    // Is this test really required ?
    if ( test_image.isContinuous() ){
        // Testing image memory is continuous.
        test_image_ptr = test_image.data;
    }

    if( test_image_ptr == NULL ){
        cout << "Error allocating image in memory. " << endl;
        exit( EXIT_FAILURE );
    }

    // Scale image to [0-1]
    vector<float> float_image;
    unsigned num_pixels = 1 * 28 * 28;
    for( size_t i = 0; i < num_pixels; ++i ){
        float_image.push_back( (float)test_image_ptr[i] / 255 );
    }

    feedBlob( &float_image[0], imagesBlobName );
    caffeNet->Forward();

    return getBlobContent( probBlobName );
}


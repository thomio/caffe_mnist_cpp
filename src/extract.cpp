#include "MNISTData.hpp"
#include <boost/program_options.hpp>
#include <iostream>
#include <string>


namespace po = boost::program_options;
using namespace std;



po::variables_map program_options( const int argc, const char *const argv[] )
{
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "Usage: ./extract -d ../dataset")
        ("dataset,d", po::value<string>(), "Path to dataset directory");

    po::variables_map args;
    po::store( po::parse_command_line(argc, argv, desc), args );
    po::notify(args);

    if( args.count("help") || argc != 3 ){
        cout << desc << endl;
        exit( EXIT_FAILURE );
    }

    cout << endl;
    if( args.count("dataset") ){
        cout << "Dataset directory: " << args["dataset"].as<string>() << endl;
    }

    return args;
}



int main( int argc, const char *argv[] )
{
    po::variables_map args = program_options( argc, argv );

    string dataset_path = args["dataset"].as<string>();
    MNISTData *mnist_data = new MNISTData( dataset_path );
    delete mnist_data;

    return 0;
}

#ifndef MNIST_DATA_HPP
#define MNIST_DATA_HPP

#include <boost/shared_ptr.hpp>
#include <string>



struct IDXFormat{
    static unsigned reverse_bits( const unsigned &x );
    static unsigned read_magic_number( const unsigned &x );
};


struct ImagesData
{
    ImagesData( const std::string &dataset_path, const std::string &file_name );
    ~ImagesData();

    void loadImages();
    void saveImages();
    void printImages();

    std::string datasetPath, fileName;
    unsigned numImages, numCols, numRows;
    float *images;
};


struct LabelsData
{
    LabelsData( const std::string &dataset_path, const std::string &file_name );
    ~LabelsData();

    void loadLabels();
    void saveLabels();

    std::string datasetPath, fileName;
    unsigned numLabels;
    float *labels;
};


struct MNISTData{
    MNISTData(const std::string &dataset_path );

    boost::shared_ptr<ImagesData> trainImages;
    boost::shared_ptr<ImagesData> testImages;

    boost::shared_ptr<LabelsData> trainLabels;
    boost::shared_ptr<LabelsData> testLabels;
};


#endif

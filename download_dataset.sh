#!/bin/bash

dataset_dir='./dataset'
if [ -d $dataset_dir ]; then
  rm -r $dataset_dir
fi

echo "Creating dataset directory."
mkdir -p $dataset_dir

echo "Downloading source files from:"
echo "http://yann.lecun.com/exdb/mnist/index.html"

train="http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz"
test="http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz"
train_labels="http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz"
test_labels="http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz"

wget --directory-prefix=$dataset_dir $train
code=$?
if [ $code -ne 0 ]; then
  echo "Error downloading file $train."
  echo "Try again !!!" 
  exit
fi

wget --directory-prefix=$dataset_dir $test
code=$?
if [ $code -ne 0 ]; then
  echo "Error downloading file $test."
  echo "Try again !!!" 
  exit
fi

wget --directory-prefix=$dataset_dir $train_labels
code=$?
if [ $code -ne 0 ]; then
  echo "Error downloading file $train_labels."
  echo "Try again !!!" 
  exit
fi

wget --directory-prefix=$dataset_dir $test_labels
code=$?
if [ $code -ne 0 ]; then
  echo "Error downloading file $test_labels."
  echo "Try again !!!" 
  exit
fi


echo "Uncompressing files..."
for entry in "$dataset_dir"/*
do
  gunzip $entry
done

echo "Done !!!"
